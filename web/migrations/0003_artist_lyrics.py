# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_abstractmodel_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='Artist',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('name', models.TextField()),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='Lyrics',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('title', models.TextField()),
                ('lyrics', models.TextField()),
                ('artist', models.ForeignKey(to='web.Artist')),
            ],
            bases=('web.abstractmodel',),
        ),
    ]
