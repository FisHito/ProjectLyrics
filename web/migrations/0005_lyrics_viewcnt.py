# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_auto_20150901_1239'),
    ]

    operations = [
        migrations.AddField(
            model_name='lyrics',
            name='viewCnt',
            field=models.IntegerField(default=0),
        ),
    ]
