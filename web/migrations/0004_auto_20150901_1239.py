# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0003_artist_lyrics'),
    ]

    operations = [
        migrations.RenameField(
            model_name='lyrics',
            old_name='lyrics',
            new_name='lyrics_text',
        ),
    ]
