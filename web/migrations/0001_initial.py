# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AbstractModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_public', models.CharField(default=b'y', max_length=1)),
                ('ipaddr', models.GenericIPAddressField(null=True)),
                ('created', models.DateTimeField(auto_now=True)),
                ('modified', models.DateTimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('title', models.TextField()),
                ('contents', models.TextField()),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='ArticleCategoryRls',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('article', models.ForeignKey(to='web.Article')),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='ArticleTagRls',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('name', models.TextField()),
                ('p_category', models.ForeignKey(to='web.Category')),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('name', models.TextField()),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('googleToken', models.TextField(null=True, blank=True)),
                ('name', models.TextField()),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='ViewCount',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('article', models.ForeignKey(to='web.Article')),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('abstractmodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.AbstractModel')),
                ('voteType', models.CharField(max_length=1)),
                ('user', models.ForeignKey(to='web.User')),
            ],
            bases=('web.abstractmodel',),
        ),
        migrations.AddField(
            model_name='articletagrls',
            name='article',
            field=models.ForeignKey(to='web.User'),
        ),
        migrations.AddField(
            model_name='articletagrls',
            name='tag',
            field=models.ForeignKey(to='web.Tag'),
        ),
        migrations.AddField(
            model_name='articlecategoryrls',
            name='category',
            field=models.ForeignKey(to='web.Category'),
        ),
        migrations.AddField(
            model_name='article',
            name='writer',
            field=models.ForeignKey(to='web.User'),
        ),
    ]
