from django.shortcuts import render, redirect
import os  

class AbstractView():
    request = None
    template = None
    paramss = {}
    resultParams = {}
    redirectUrl = None
 
    def __init__(self, _request=None, _template=None):
        self.setRequest(_request)  
        resultParams = {}

        if _template is not None:
           self.setTemplate(_template)

    def setRequest(self, _request):
        self.request = _request

    def setTemplate(self, _template):
        self.template = _template 

    def addResultParam(self, _key, _value):
        self.resultParams[_key] = _value 

    def isGet(self, _request):
        return (_request.method == 'GET')

    def isPost(self, _request):
        return (_request.method == 'POST')

    def getParam(self, _key):
        if self.request is not None:
            if self.isGet(self.request):
                return self.request.GET.get(_key, False);
            if self.isPost(self.request): 
                return self.request.POST.get(_key, False);
        else:
                if self.params.has_key(_key):
                    return self.params[_key]
                return False
    def save(self):
        return False

    def list(self):
        return False

    def view(self):
        return False

    def setResultParams(self, _params):
        self.resultParams = _params
    
    def getResultParams(self):
        if self.proc():
            return self.resultParams
        else:
            return None

    def setParams(self, _params):
        self.params = _params

    def getRender(self): 
        try:
            if self.redirectUrl is not None :
                return redirect(self.redirectUrl)
            return render(self.request, self.template, self.resultParams)
        except Exception as e:  
            print str(e) 