from django.shortcuts import render
from AbstractView import AbstractView 
from web.models import *

class LyricsView(AbstractView):
    template = 'web/index.html'  

    def save(self): 
        am = ArtistManager()    
        lm = LyricsManager()
        artist = am.select({'artist': self.getParam('artist').strip()}) 

        if artist is not None and len(artist) > 0 :
            artist = artist[0]
        else:
            artist = am.insert({'name': self.getParam('artist').strip()}) 

        if self.getParam('id') is False:   
            lyrics = lm.insert({'title':self.getParam('title').strip(),'lyrics':self.getParam('lyrics'),'artist':artist})
        else : 
            lyrics = lm.update({'id':self.getParam('id'),'title':self.getParam('title').strip(),'lyrics':self.getParam('lyrics'),'artist':artist})

        self.addResultParam('artists', Artist.objects.all())
        self.addResultParam('lyricsList',lm.select({'artist_id': lyrics.artist.id}).order_by('-viewCnt'))
        self.addResultParam('lyrics',lyrics)
        
        return True          

    def list(self):
        lm = LyricsManager() 
        self.addResultParam('artists', Artist.objects.all()) 
        self.addResultParam('lyricsList', lm.select({'artist_id': self.getParam('no')}).order_by('-viewCnt'))
        return True

    def view(self):
        lm = LyricsManager()  
        lyrics = lm.select_by_pk(self.getParam('no')) 
        lyrics.view()
        self.addResultParam('artists', Artist.objects.all())
        self.addResultParam('lyricsList', lm.select({'artist_id': lyrics.artist.id}).order_by('-viewCnt'))
        self.addResultParam('lyrics', lyrics)
        return True 