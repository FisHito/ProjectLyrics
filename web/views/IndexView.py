from django.shortcuts import render
from AbstractView import AbstractView
from web.models import *

class IndexView(AbstractView):
    template = 'web/index.html' 

    def list(self):  
        aa = Artist.objects.all().order_by('-created')[:10]
        bb = Lyrics.objects.all().order_by('-created')[:10]
        cc = Lyrics.objects.all().order_by('-created', '-viewCnt')[:10]
        dd = Lyrics.objects.all().order_by('-viewCnt')[:10]

        #cc = Artist.objects.raw('''
        #    SELECT artist FROM LYRICS
        #    ''') 
 
        self.addResultParam('artists', Artist.objects.all())
        self.addResultParam('lyricsList', None)
        self.addResultParam('lyrics', None)
        self.addResultParam('aa', aa)
        self.addResultParam('bb', bb)
        self.addResultParam('cc', cc)
        self.addResultParam('dd', dd)
        return True
 
