from django.shortcuts import render
from AbstractView import AbstractView
from web.models import *
from IndexView import IndexView

class RegisterView(AbstractView):
    template = 'web/index.html' 
    redirectUrl = '/'

    def save(self): 
        um = UserManager()
        um.insert({'googleToken':self.getParam('googleToken'),'name':self.getParam('name')})
      
        return True
