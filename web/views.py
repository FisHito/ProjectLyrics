#-*- coding: utf-8 -*-
import sys
sys.path.insert(0, 'web/services')
sys.path.insert(0, 'web/views')  
from django.shortcuts import render
from ArticleService import ArticleService
from IndexView import IndexView
from RegisterView import RegisterView
from LyricsView import LyricsView
from ArtistView import ArtistView
from web.models import *

import re, urllib2, os

def isGet(request):
    return (request.method == 'GET')

def isPost(request):
    return (request.method == 'POST')

def striphtml(data):
    p = re.compile(r'<.*?>')
    return p.sub('', data)

def index(request):   
    if isGet(request): 
        indexView = IndexView(request)   
        if indexView.list():
            return indexView.getRender() 
        else:
            return None 
    elif isPost(request):
        pass

def register(request):   
    if isGet(request):
        pass
  
    elif isPost(request):
        registerView = RegisterView(request)  
        if registerView.save(): 
          return registerView.getRender() 
        else:
            return None
def insert_artist(request):    
    if isGet(request):
        pass
  
    elif isPost(request):
        artistView = ArtistView(request)   
        if artistView.save():
            return artistView.getRender()  
        else:
            return None

def save_lyrics(request):    
    if isGet(request):
        pass 
    elif isPost(request):
        lyricsView = LyricsView(request)   
        if lyricsView.save():
            return lyricsView.getRender() 
        else:
            return None 
            
def list_lyrics(request):
    if isGet(request):
        lyricsView = LyricsView(request)   
        if lyricsView.list():
            return lyricsView.getRender() 
        else:
            return None        
    elif isPost(request):
        pass

def view_lyrics(request):
    if isGet(request):
        lyricsView = LyricsView(request)   
        if lyricsView.view():
            return lyricsView.getRender() 
        else:
            return None 
    elif isPost(request):
        pass

def update_lyrics(request):  
    return index(request)