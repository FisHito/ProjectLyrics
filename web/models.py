#-*- coding: utf-8 -*-
from django.db import models

IS_PUBLIC = 'y'
IS_PRIVATE = 'n'
IS_GOOD = 'g'
IS_BAD = 'b'

class AbstractModel(models.Model):
    is_public = models.CharField(default=IS_PUBLIC, max_length=1, null=False)
    status =  models.CharField(max_length=1, null=True)
    ipaddr = models.GenericIPAddressField(null=True)
    created = models.DateTimeField(auto_now=True)
    modified = models.DateTimeField(null=True)

    def inactivate(self):
        self.is_public = IS_PRIVATE
        self.save() 

class AbstractManager():
    model = None

    def inactivate(self, _id): #True/False
        obj = self.model.objects.get(pk=_id) 
        if obj is not None:
            obj.inactivate()
 
    def delete(self, params): #True/False
        obj = self.model.objects.get(pk=_id)
        if obj is not None:
            obj.delete()
 
    def select_by_pk(self, _id): #Result/None
        return self.model.objects.get(pk=_id) 

class User(AbstractModel):
    def __unicode__(self):
        return self.googleToken

    googleToken = models.TextField(null=True, blank=True)
    name = models.TextField(null=False) 
 
class UserManager(AbstractManager):
    model = User
    def insert(self, params): #Pk/None
        if len(User.objects.filter(googleToken=params['googleToken'])) == 0:
            u = self.model(googleToken=params['googleToken'], name=params['name'])
            u.save()

    def update(self, params): #Pk/None
        obj = self.model.objects.get(pk=_id)
        if obj is not None:
            if params['googleToken'] != None:
                obj.googleToken = params['googleToken']
            if params['name'] != None:
                obj.name = params['name']
            obj.save()

    def delete(self, params): #True/False
        obj = self.model.objects.get(pk=_id)
        if obj is not None:
            obj.inactivate()

    def select(self, params): #Result/None
        return self.model.objects.all()
 
class Article(AbstractModel):
    title = models.TextField(null=False) 
    contents = models.TextField(null=False) 
    writer = models.ForeignKey(User) 

class Category(AbstractModel):
    name = models.TextField(null=False) 
    p_category = models.ForeignKey('Category') 

class Tag(AbstractModel):
    name = models.TextField(null=False) 

class Vote(AbstractModel): 
    voteType = models.CharField(max_length=1, null=False)
    user = models.ForeignKey(User)

class ViewCount(AbstractModel):
    article = models.ForeignKey(Article)


class ArticleTagRls(AbstractModel):
    article = models.ForeignKey(User) 
    tag = models.ForeignKey(Tag) 

class ArticleCategoryRls(AbstractModel):
    article = models.ForeignKey(Article) 
    category = models.ForeignKey(Category) 

class Artist(AbstractModel):
    name = models.TextField(null=False) 

class ArtistManager(AbstractManager):
    model = Artist
    def insert(self, params):
        if len(self.model.objects.filter(name=params['name'])) == 0:
            m = self.model(name=params['name'])
            m.save()
            return m

    def select(self, params):
        if params['artist'] is not None:
            return self.model.objects.filter(name=params['artist'])

        return self.model.objects.all()

class Lyrics(AbstractModel):
    title = models.TextField(null=False)
    lyrics_text = models.TextField(null=False)
    artist = models.ForeignKey(Artist)  
    viewCnt = models.IntegerField(default=0)

    def view(self):
        self.viewCnt += 1
        self.save()

class LyricsManager(AbstractManager):
    model = Lyrics
    def insert(self, params):
        obj = self.model.objects.filter(title=params['title'], artist=params['artist'])
        if len(obj) == 0: 
            m = self.model(title=params['title'],lyrics_text=params['lyrics'],artist=params['artist'])
            m.save()
            return m
        else: 
            obj[0].lyrics_text=params['lyrics']
            obj[0].save()
            return obj[0]  

    def update(self, params): 
        m = self.model.objects.get(pk=params['id'])
        m.title=params['title']
        m.lyrics_text=params['lyrics']
        m.artist=params['artist']
        m.save()
        return m
  
    def select(self, params):
        if params.has_key('artist_id') is not None:
            print params['artist_id']
            return self.model.objects.filter(artist=Artist.objects.get(pk=params['artist_id']))
        return self.model.objects.all()