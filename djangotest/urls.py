from django.conf.urls import include, url
from django.contrib import admin
from web import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'djangotest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index),
    url(r'^register$', views.register),

    url(r'^insert/artist$', views.insert_artist), 
    url(r'^save/lyrics$', views.save_lyrics), 

    url(r'^artist$', views.list_lyrics),
    url(r'^lyrics$', views.view_lyrics), 

    url(r'^ul$', views.update_lyrics), 

]
